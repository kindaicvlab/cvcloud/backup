FROM ubuntu:20.04

ENV USER_NAME user
ENV USER_NAME_UID 1000
RUN useradd -m -u ${USER_NAME_UID} ${USER_NAME}

RUN apt-get update \
 && apt-get install -y --no-install-recommends rsync \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/*
